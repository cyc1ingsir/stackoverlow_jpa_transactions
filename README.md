### Sampel StackOverflow question
## Spanning a transaction over multiple database calls

This repository is meant to provide a full example for a question asked
on SO.

Build and run it with
```
./gradlew build
./gradlew bootRun
```

Running it via `bootRun` will call the UpdateService once right after startup.  
The default configuration will try to connect to a MySQL database (setup via `docker-compose up` run from within the docker folder.
The tests (see `ChairUpdaterTest`) will talk to an embedded H2 (with no further setup needed).

Currently one finds multiple commented out code blocks reflecting various approaches having been made to tackle the problem. 
