package org.example.jpatrans;


import org.example.jpatrans.persistence.ChairRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

import static org.junit.Assert.assertEquals;

/**
 * Created by cyc1ingsir on 14/12/18
 */
@Slf4j
@RunWith(SpringRunner.class)
@DataJpaTest
@Import(ChairUpdaterService.class)
@Transactional
public class ChairUpdaterTest {

    private static final int COUNT_AFTER_ROLLBACK_AND_ONE_ADDED = 3;

    @Autowired
    private ChairUpdaterService updater;

    @Autowired
    private ChairRepository repository;

    @Before
    public void setup() {
        updater.initializeChairs();
        updater.printAllChairs();
    }

    @After
    public void tearDown() {
        updater.printAllChairs();
    }

    @Test
    public void positiveTest() throws UpdatingException {
        updater.updateChairs(3, 10);
    }

    @Test
    public void testRollingBack() {

        // Trying to update with an invalid element
        // to force rollback
        try {
            updater.updateChairs(3, null);
        } catch (Exception e) {
            LOGGER.info("Rolled back?", e);
        }

        // Adding a valid element after the rollback
        // should succeed and add an additional element
        updater.addChair(4, 10);
        assertEquals(COUNT_AFTER_ROLLBACK_AND_ONE_ADDED, repository.findAll().spliterator().getExactSizeIfKnown());
    }
}
