package org.example.jpatrans;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.jpatrans.persistence.Chair;
import org.example.jpatrans.persistence.ChairRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by cyc1ingsir on 14/12/18
 */
@Slf4j
@Service
@AllArgsConstructor
@Transactional
public class ChairUpdaterService {

    private ChairRepository repository;

    /*
     * Initialize the data store with some
     * sample data
     */
    public void initializeChairs() {

        repository.deleteAll();
        Chair chair4 = new Chair(1, 4);
        Chair chair3 = new Chair(2, 3);

        repository.save(chair4);
        repository.save(chair3);

    }

    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = UpdatingException.class)
    public void addChair(int id, Integer legCount) {
        repository.save(new Chair(id, legCount));
    }

    public void updateChairs(int id, Integer legCount) throws UpdatingException {

        Chair chair = new Chair(id, legCount);

        doUPdate(chair);
    }

    /*
     * Expected behaviour:
     * when saving a given chair fails ->
     * deleting all other is rolled back
     */
//    @Transactional
//    @Transactional(propagation = Propagation.MANDATORY)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = UpdatingException.class)
    protected void doUPdate(Chair chair) throws UpdatingException {

        try {
            repository.deleteWithinGivenTransaction();
            repository.save(chair);
//            repository.updateAfterDelete(chair);
        } catch (Exception e) {
            LOGGER.error(" |-------- catched EXCEPTION --------------| ");
            throw new UpdatingException(e.getMessage());
        }
    }

    public void printAllChairs() {
        Iterable<Chair> chairs = repository.findAll();
        LOGGER.info(" ----------------------------------------------------------");
        for (Chair chair :
                chairs) {
            LOGGER.info(">> >> Chair with {} legs", chair.getLegs());
        }
        LOGGER.info("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
    }


}
