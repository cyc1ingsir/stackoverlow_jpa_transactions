package org.example.jpatrans.persistence;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by cyc1ingsir on 14/12/18
 */
@Entity
@Table(name = "chair")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Chair {

    // Not auto generating the id is on purpose
    // for later testing with non unique keys
    @Id
    private int id;

    @Column(name = "legs", nullable = false)
    private Integer legs;
}
