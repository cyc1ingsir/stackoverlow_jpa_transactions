package org.example.jpatrans.persistence;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by cyc1ingsir on 14/12/18
 */
@Repository
@Transactional(readOnly = true)
public interface ChairRepository extends CrudRepository<Chair, Integer> {

    /*
     * Create an own delete function to make change in behaviour
     * explicit
     */
    @Modifying
    @Transactional(propagation = Propagation.MANDATORY)
    @Query("DELETE from Chair ")
    void deleteWithinGivenTransaction();

/*    Experimenting
 *        with doing the update after delete with a native query
 */
//    @Modifying
//    @Query(value = "delete from Chair; insert :chair into Chair;", nativeQuery = true)
//    @Query(value = "delete from chair; insert into chair values(5, 20);", nativeQuery = true)
//    void updateAfterDelete(@Param("chair") Chair chair)

    /* Overriding a provided Method to change transactional behaviour */
//    @Override
//    @Modifying
//    @Transactional(propagation = Propagation.REQUIRED)
//    <S extends Chair> S save(S entity);
}
