package org.example.jpatrans;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.PostConstruct;

@Slf4j
@SpringBootApplication
@EnableTransactionManagement
public class JpaTransactionTestApplication {

    @Autowired
    private ChairUpdaterService updater;

    public static void main(String[] args) {
        SpringApplication.run(JpaTransactionTestApplication.class, args);
    }

    @PostConstruct
    public void runOnStart() {

        updater.initializeChairs();

        try {
            updater.updateChairs(4, null);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

        updater.addChair(10,20);

        updater.printAllChairs();

    }

}

