package org.example.jpatrans;

/**
 * Created by cyc1ingsir on 14/12/18
 */
public class UpdatingException extends Exception{

    private static final long serialVersionUID = -2868937694760937691L;

    public UpdatingException(String message) {
        super(message);
    }
}
