package org.example.jpatrans;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Created by cyc1ingsir on 14/12/18
 */
@Slf4j
@Service
@AllArgsConstructor
public class UpdateService {

    private final ChairUpdaterService updater;

    public void initChairs() {
        updater.initializeChairs();
        updater.printAllChairs();
    }

    /*
     * This function is the equivalent to the rollback test
     * within @see ChairUpdaterTest#testRollingBack()
     */
    public void updateChairs() {

        try {
            updater.updateChairs(4, null);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

        updater.addChair(3, 10);
        updater.printAllChairs();
    }

}
